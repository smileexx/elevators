var PROCESS = {
    init: function (elevators, floors) {
        var eCount = elevators.length;
        var fCount = floors.length;
/*

        var sortOrder = function( L ) {
            var Q = L.destinationQueue;
            for( var i = Q.length -1; i >= 0; i-- ) {
                for( var j = 0; j < i; j++ ){
                    var k = Q[j+1];
                    if( Q[j] > k ) {
                        Q[j+1] = Q[j]
                        Q[j] = k;
                    }
                }
            }

            L.destinationQueue = Q;
            L.checkDestinationQueue();
        }
*/
        var sortAsc = function ( a, b ) {
            return a - b;
        };

        var sortDesc = function ( a, b ) {
            return b - a;
        };


        var List = {
            up  : [],
            down: [],

            add : function(  floor, dir ) {
                if ( this[ dir ].indexOf( floor ) < 0 ) {
                    this[ dir ].push( floor );
                    /*if ( dir == 'up' ) {
                        this[ dir ].sort( sortAsc );
                    } else {
                        this[ dir ].sort( sortDesc );
                    }*/
                    console.log( 'UP', this.up, 'DOWN', this.down );
                }
            },

            sp: function ( floor, dir ) {
                var i = this[ dir ].indexOf( floor );
                return this[ dir ].splice(i, 1).pop();
            },

            getMaxFloor: function ( curFloor ) {
                return Math.max( Math.max.apply(null, this.up), Math.max.apply(null, this.down), curFloor );
            },

            getMinFloor: function ( curFloor ) {
                return Math.min( Math.min.apply(null, this.up), Math.min.apply(null, this.down), curFloor );
            },

            getOne: function () {
                var one = null;

                if( this.up && this.up.length ){
                    one = this.up.shift();
                } else if( this.down && this.up.down ) {
                    one = this.down.shift();
                }

                return one;
            }

        };

        var setDirection = function( lift, direction ) {
            lift.dir = direction;
            if( 'up' == lift.dir ) {
                lift.goingUpIndicator(true);
                lift.goingDownIndicator(false);
            } else if ( 'down' == lift.dir ) {
                lift.goingUpIndicator(false);
                lift.goingDownIndicator(true);
            } else {
                // on idle
                lift.goingUpIndicator(true);
                lift.goingDownIndicator(true);
            }
            return lift.dir;
        };

        var choseDirection = function ( curFloor, newFloor ){
            return curFloor < newFloor; // true = UP
        };

        var move = function() {
            elevators.forEach(function ( lift, index ) {
                // actual schedule
                var actual = lift.destinationQueue;
                // pressed buttons
                var pressed = lift.getPressedFloors();
                var curFloor = lift.currentFloor();

                if( !(actual.length && pressed.length) ) {
                    var floor = List.getOne();
                    if( !floor ) {
                        // no tasks
                        return false;
                    }

                    setDirection(lift, choseDirection( curFloor, floor ));
                    if( 'up' == lift.dir ) {
                        var maxFloor = List.getMaxFloor( curFloor );
                    } else {

                    }


                } else {
                    // handle existing list first
                }
                // delete current floor from task

            })
        };

        // ==================== INIT & EVENTs ========================
        for( var i = 0; i < eCount; i++ ) {
            var lift = elevators[i];

            setDirection( lift, null );

            lift.on("idle", function() {
                //TODO check tasks in FIFO
                setDirection( this, null );
            });
            lift.on("floor_button_pressed", function(floorNum) {
                console.log( 'Lift btn: ', floorNum );
                // this.goToFloor(floorNum);
            });
            lift.on("passing_floor", function(floorNum, direction) {
                console.log( 'Passing floor: ',floorNum, direction );
            });
            lift.on("stopped_at_floor", function(floorNum) {
                console.log( 'At floor: ',floorNum );
            });
        }

        for( var i = 0; i < fCount; i++ ) {
            var floor = floors[i];
            floor.on("up_button_pressed", function() {
                console.log( 'Floor ', this.floorNum(), ' UP btn' );
                List.add( this.floorNum(), 'up' );
            });
            floor.on("down_button_pressed", function() {
                console.log( 'Floor ', this.floorNum(), ' DOWN btn' );
                List.add( this.floorNum(), 'down' );
            })
        }
    },
    update: function(dt, elevators, floors) {
        // We normally don't need to do anything here
        /*for( var i = 0; i < elevators.length; i++ ) {
            var lift = elevators[ i ];
            if( lift.direction != lift.destinationDirection() ) {
                lift.direction = lift.destinationDirection();
            }
        }*/
    }
}

/*

 elevator.maxPassengerCount()

 elevator.destinationDirection()

 elevator.destinationQueue = [];
 elevator.checkDestinationQueue();

 elevator.currentFloor()

 elevator.goToFloor( 0 );

 elevator.goingUpIndicator()

 elevator.goingDownIndicator()

 elevator.loadFactor()

 elevator.getPressedFloors()


user weight = 1 / lift.maxPassengerCount()
elevator users count = Math.ceil( elevator.loadFactor() / ( 1 / lift.maxPassengerCount() ) )
 */